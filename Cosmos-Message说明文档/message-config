message-config
===

> 这个项目主要是用来使用配置文件的，配置文件支持`键值对`和`文件`，`键值对`类型的支持文件格式为`properties`、`ini`，`文件`类型支持任意文件

### 配置文件目录结构

> ###请注意：
> - [D]：文件夹
> - demo、sample：这两个文件夹内的结构与default一致
> - files/datasource/rdbms.properties存储的是数据源的信息，具体格式将在message-jdbc中讲解
> - file/error/framework.properties存储的是异常信息，具体格式将在message-exception中讲解
> - files/log4j.properties是log4j的配置，具体请参考log4j的配置

    -- cosmos-configs[D]
        -- default[D]
            -- files[D]
                -- datasource[D]
                    -- rdbms.properties
                -- error[D]
                    -- framework.properties
                -- log4j.properties
            -- i18n[D]
                -- base[D]
                    -- base.properties
                    -- base_en_US.properties
                -- user[D]
                    -- user.properties
                    -- user_en_US.properties
            -- test.ini
            -- test.properties
        -- demo[D]
        -- sample[D]
        -- root.properties

### 使用的配置
> 支持三种方式配置，优先级如下：
> 1. 默认是在web.xml中配置
> 2. 启动参数获取
> 3. 环境变量获取

1. 在web.xml中设置
   在web.xml的最上方设置listener，并设置context-param
    ```xml
    <!-- 读取配置文件 start -->
    <listener>
        <listener-class>message.config.core.ParamsHomeListener</listener-class>
    </listener>
    <context-param>
        <param-name>paramsHome</param-name>
        <param-value>F:\workspace\workspace\messageboard\knowledge\knowledge-setting</param-value>
    </context-param>
    <!-- 读取配置文件 end -->
    ```
2. 在启动参数中设置
    在tomcat或者其他web容器中设置params.home=配置文件路径。
3. 在环境变量中设置
    在服务器中设置环境变量PARAMS_HOME为配置文件路径。

> ###注意：
> 在cosmos-configs目录下，会有N多个文件夹和一个root.properties文件，root.properties文件中有一个config.root的键值对，这里是配置指定使用哪一组配置，值为N多个文件夹中的一个。这样能实现多个环境自由切换。

### 如何在代码中使用
> 支持三种文件类型的配置读取，如下：
> 1. 获取properties文件中的配置、文件resource的获取
> 2. 获取国际化资源配置
> 3. 获取ini文件的配置

1. 获取properties文件中的配置、文件resource的获取
    使用类`message.config.SystemConfig`，具体方法请参考源码。注意：方法`getConfigFile`是用来获取${PARAM_PATH}/files下的文件资源，如${PARAM_PATH}/files/demo.txt，则传入参数demo.txt；如${PARAM_PATH}/files/demo/demo.txt，则传入参数demo/demo.txt。
2. 获取国际化资源配置
    使用类`message.config.MessageUtils`，具体方法请参考源码。
3. 获取ini文件的配置
    使用类`message.config.IniConfig`，具体方法请参考源码。

##依赖的cosmos项目
- message-base

