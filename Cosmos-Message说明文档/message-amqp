message-amqp
===

> 这个项目是对spring-amqp的一个封装，增加了序列化的一些处理、对异常的处理等等

####如何使用此项目
1. 在web项目`web.xml`文件的spring配置处处写上如下的代码：
    ```
    <!-- 以下每个项目都要如此配置web.xml -->
    <!-- spring配置监听器 -->
    <listener>
        <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
    </listener>
    <!-- spring上下文配置文件，加载classpath/META-INF/spring/下的xml文件 -->
    <context-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>classpath*:/META-INF/spring/*.xml</param-value>
    </context-param>
    <servlet>
        <servlet-name>spring</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <!-- spring web容器中的配置 -->
            <param-value>classpath*:/META-INF/springWeb/*.xml</param-value>
        </init-param>
    </servlet>
    <servlet-mapping>
        <servlet-name>spring</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>
    ```
2. 在服务提供方的spring.xml配置文件中：
    ```
    <?xml version="1.0" encoding="UTF-8"?>
    <beans xmlns="http://www.springframework.org/schema/beans"
           xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
           xmlns:message-amqp="http://www.message.com/schema/message-amqp"
           xmlns:context="http://www.springframework.org/schema/context"
           xsi:schemaLocation="
        http://www.message.com/schema/message-amqp
        http://www.message.com/schema/message-amqp/message-amqp-1.0.xsd
        http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
        http://www.springframework.org/schema/context
            http://www.springframework.org/schema/context/spring-context-2.5.xsd">

        <!-- spring注解的包扫描 -->
        <context:annotation-config></context:annotation>
        <context:component-scan base-package="message.mq.producter"></context:component>

        <message-amqp:rabbitMq addresses="192.168.1.101:5672"
                               username="admin"
                               password="admin123"
                               exchange="exchange.direct.default"></message>
    </beans>
    ```
3. 在消费方的spring.xml配置文件中：
    ```
    <?xml version="1.0" encoding="UTF-8"?>
    <beans xmlns="http://www.springframework.org/schema/beans"
           xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
           xmlns:message-amqp="http://www.message.com/schema/message-amqp"
           xmlns:context="http://www.springframework.org/schema/context"
           xsi:schemaLocation="
        http://www.message.com/schema/message-amqp
        http://www.message.com/schema/message-amqp/message-amqp-1.0.xsd
        http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
        http://www.springframework.org/schema/context
            http://www.springframework.org/schema/context/spring-context-2.5.xsd">

        <!-- spring注解的包扫描 -->
        <context:annotation-config></context:annotation>
        <context:component-scan base-package="message.mq.consumer"></context:component>

        <!-- 初始化定义 -->
        <message-amqp:rabbitMq addresses="192.168.1.101:5672"
                               username="admin"
                               password="admin123"
                               exchange="exchange.direct.default"></message>

        <!-- 定义队列 -->
        <message-amqp:queue-def>
            <message-amqp:queue ref="testConsumer" method="receive1" exchange="exchange.direct.default"
                                queue-name="queue.demo.consumer" routing-key="testRoutingKey1"></message>
            <message-amqp:queue ref="testConsumer" method="receive2" exchange="exchange.direct.default"
                                queue-name="queue.demo.consumer2" routing-key="testRoutingKey2"></message>
            <message-amqp:queue ref="testConsumer" method="receive3" exchange="exchange.direct.default"
                                queue-name="queue.demo.consumer3" routing-key="testRoutingKey3"></message>
        </message-amqp:queue-def>
    </beans>
    ```
4. 代码中使用
    - 提供方使用
        ```java
        @Component
        public class TestProducter {
            @Autowired
            private RabbitTemplate template;

            @Autowired
            @Qualifier("defaultDirectExchange")
            private DirectExchange exchange;

            public void send1(List<Demo> demos) {
                this.template.convertAndSend(this.exchange.getName(), "testRoutingKey", demos);
                System.out.println("已发送");
            }

            public void send2(Demo demo) {
                this.template.convertAndSend(this.exchange.getName(), "testRoutingKey2", demo);
                System.out.println("已发送2");
            }

            public void send3(Map<String, String> params) {
                this.template.convertAndSend(this.exchange.getName(), "testRoutingKey3", params);
                System.out.println("已发送3");
            }
        }
        ```
    - 使用方使用
        ```
        @Component
        public class TestConsumer {
            public void receive1(List<Demo> demos) {
                System.out.println("监听准备：");
                System.out.println("get demo is: " + JsonUtils.toString(demos));
                System.out.println("get demo one name is : " + demos.get(0).getName());
            }

            public void receive2(Demo demo) {
                System.out.println("监听准备：");
                System.out.println("get demo is: " + JsonUtils.toString(demo));
                System.out.println("get demo one name is : " + demo.getName());
            }

            public void receive3(Map<String, String> demo) {
                System.out.println("监听准备：");
                System.out.println("get demo is: " + JsonUtils.toString(demo));
                System.out.println("get Map one name is : " + demo.get("key1"));
            }
        }
        ```
    - 单元测试
        - 服务器端测试（发送一条消息）
            ```
            @Test
            public void send1() {
                List<Demo> demos = new ArrayList<Demo>();

                Demo demo1 = new Demo();
                demo1.setPkId(1L);
                demo1.setName("sunhao");
                demos.add(demo1);

                Demo demo2 = new Demo();
                demo2.setPkId(1L);
                demo2.setName("sunhao");
                demos.add(demo2);

                this.testProducter.send1(demos);
            }

            @Test
            public void send2() {
                Demo demo = new Demo();
                demo.setPkId(10L);
                demo.setName("sunhao2");

                this.testProducter.send2(demo);
            }

            @Test
            public void send3() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("key1", "value1");
                params.put("key2", "value2");

                this.testProducter.send3(params);
            }
            ```
        - 消费方测试（启动进程，监听收到的消息）
            ```
            // 启动即可
            @Test
            public void receive() throws InterruptedException {
                System.out.println("监听准备：");
                for (; ; ) {
                    Thread.currentThread().sleep(1000);
                }
            }
            ```
    - 总结
        两边定义好相同的pojo对象即可，不需要互相依赖，包路径、类名也不需要一样。唯一需要一致的是字段名和类型。

##依赖的cosmos项目
- message-template
- message-utils
- message-config

